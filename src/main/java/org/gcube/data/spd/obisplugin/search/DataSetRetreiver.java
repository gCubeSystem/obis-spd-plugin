package org.gcube.data.spd.obisplugin.search;

import java.util.List;
import java.util.Map;

import org.gcube.data.spd.model.products.DataProvider;
import org.gcube.data.spd.model.products.DataSet;
import org.gcube.data.spd.obisplugin.Constants;
import org.gcube.data.spd.obisplugin.search.query.MappingUtils;
import org.gcube.data.spd.obisplugin.search.query.QueryByIdentifier;
import org.gcube.data.spd.obisplugin.search.query.QueryType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataSetRetreiver {

	private static Logger log = LoggerFactory.getLogger(DataSetRetreiver.class);

	@SuppressWarnings("unchecked")
	public static DataSet get(String baseURL, String key) throws Exception {
		log.debug("Dataset Get: [ key={}, baseURL={} ]", key, baseURL);
		DataSet dataset = new DataSet(key);
		long start = System.currentTimeMillis();
		QueryByIdentifier datasetQuery = new QueryByIdentifier(baseURL, key, QueryType.Dataset);
		Map<String, Object> listMaps = MappingUtils.getObjectMapping(datasetQuery.build());
		log.debug("Dataset Retrieved: " + listMaps);
		if (listMaps != null) {
			List<Map<String, Object>> results = (List<Map<String, Object>>) listMaps.get("results");
			if (results != null && !results.isEmpty()) {
				Map<String, Object> mapping = results.get(0);
				log.debug("Dataset Name: " + MappingUtils.getAsString(mapping, "title"));
				dataset.setName(MappingUtils.getAsString(mapping, "title"));

				String citation = MappingUtils.getAsString(mapping, "citation");

				if (citation == null) {
					List<Map<String, Object>> institutionMapping = (List<Map<String, Object>>) mapping
							.get("institutes");
					if (institutionMapping != null && !institutionMapping.isEmpty()) {
						if (MappingUtils.getAsString(institutionMapping.get(0), "name") != null)
							citation = MappingUtils.getAsString(institutionMapping.get(0), "name");
						dataset.setCitation(citation);
					}
				} else {
					dataset.setCitation(citation);
				}

				List<Map<String, Object>> providerMapping = (List<Map<String, Object>>) mapping.get("contacts");
				DataProvider provider = null;
				if (providerMapping != null && !providerMapping.isEmpty()) {
					for (Map<String, Object> contact : providerMapping) {
						String contactType = MappingUtils.getAsString(contact, "type");
						if (contactType != null && !contactType.isEmpty()
								&& contactType.compareToIgnoreCase("creator") == 0) {
							Integer providerKey = MappingUtils.getAsInteger(contact, "organization_oceanexpert_id");
							String providerName = MappingUtils.getAsString(contact, "organization");
							provider = new DataProvider(providerKey.toString());
							provider.setName(providerName);
							break;
						}

					}
				} else {
					provider=new DataProvider(Constants.REPOSITORY_NAME);
					provider.setName(Constants.REPOSITORY_NAME);
				}
				dataset.setDataProvider(provider);
			}
		}
		log.trace("[Benchmark] time to retrieve dataset is " + (System.currentTimeMillis() - start));
		return dataset;
	}

}
