package org.gcube.data.spd.obisplugin.search;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public enum TaxonCategories implements Comparable<TaxonCategories> {
	// Taxonomic ranks. In capital the most used.
	// DOMAIN,
	// KINGDOM,
	// PHYLUM OR DIVISION
	// Subphylum or Subdivision
	// CLASS
	// Subclass
	// Superorder
	// ORDER
	// Suborder
	// FAMILY
	// Subfamily
	// GENUS
	// SPECIES

	DOMAIN(1), KINGDOM(2), PHYLUM(3), DIVISION(4), SUBPHYLUM(5), SUBDIVISION(6), CLASS(7), SUBCLASS(8), SUPERORDER(
			9), ORDER(10), SUBORDER(11), FAMILY(12), SUBFAMILY(13), GENUS(14), SPECIES(15);

	private int order;

	TaxonCategories(int order) {
		this.order = order;
	}

	public int getOrder() {
		return order;
	}

	public static TaxonCategories getByOrder(int position) {
		if (position < 1 || position > 15) {
			return null;
		}
		for (TaxonCategories taxon : TaxonCategories.values()) {
			if (taxon.order == position) {
				return taxon;
			}
		}
		return null;
	}

	public static TaxonCategories getParent(TaxonCategories taxon) {
		if (taxon == null) {
			return null;
		}
		if (taxon.order <= 1) {
			return null;
		} else {
			int position = taxon.order;
			position--;
			return getByOrder(position);
		}

	}

	public static TaxonCategories getTaxonCategory(String taxonRank) {
		if (taxonRank == null || taxonRank.isEmpty()) {
			return null;
		}
		String taxonR = taxonRank.toUpperCase();
		for (TaxonCategories taxon : TaxonCategories.values()) {
			if (taxon.name().compareTo(taxonR) == 0) {
				return taxon;
			}
		}
		return null;
	}

}
