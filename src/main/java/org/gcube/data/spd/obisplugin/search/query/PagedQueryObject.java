package org.gcube.data.spd.obisplugin.search.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class PagedQueryObject {

	private @NonNull String baseUri;

	@Getter
	List<QueryCondition> conditions = new ArrayList<QueryCondition>();

	private @NonNull ResultType resultType;

	private @NonNull Integer resultPerQuery;

	private String after = null;

	private Integer pageCount = 0;

	public void setConditions(QueryCondition... conditions) {
		this.conditions.addAll(Arrays.asList(conditions));
	}

	public void setAfter(String after) {
		this.after = after;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public String buildNext() {
		StringBuilder query = new StringBuilder(baseUri);
		if (!baseUri.endsWith("/"))
			query.append("/");
		query.append(this.resultType.getQueryEntry()).append("/");
		query.append("?size=").append(resultPerQuery);
		if (after != null) {
			query.append("&after=").append(after);
		}

		pageCount += resultPerQuery;

		if (conditions.size() > 0)
			for (QueryCondition queryCond : conditions)
				query.append("&").append(queryCond.getKey()).append("=").append(queryCond.getValue());
		log.debug("executed query is " + query.toString());
		return query.toString();
	}

}
