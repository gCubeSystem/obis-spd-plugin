package org.gcube.data.spd.obisplugin.search.query;

import java.io.StringReader;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class MappingUtils {

	private static final int TIMEOUT = 40000;
	private static final int retries = 5;
	private static final int SLEEP_TIME = 10000;

	private static Logger log = LoggerFactory.getLogger(MappingUtils.class);

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getObjectMapping(String query) throws Exception {

		String response = executeQuery(query);

		ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
		return mapper.readValue(new StringReader(response), Map.class);

	}

	public static List<Map<String, Object>> getObjectList(String query) throws Exception {
		String response = executeQuery(query);
		ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
		return mapper.readValue(new StringReader(response), new TypeReference<LinkedList<HashMap<String, Object>>>() {
		});

	}

	public static String getAsString(Map<String, Object> map, String key) {
		if (!map.containsKey(key))
			return null;
		return (String) map.get(key);
	}

	public static Double getAsDouble(Map<String, Object> map, String key) {
		if (!map.containsKey(key))
			return 0d;
		if (map.get(key) instanceof Double) {
			return (Double) map.get(key);
		} else {
			if (map.get(key) instanceof Integer) {
				Integer value = (Integer) map.get(key);
				return value.doubleValue();
			} else {
				if (map.get(key) instanceof String) {
					String value = (String) map.get(key);
					return Double.valueOf(value);
				} else {
					return 0d;
				}
			}
		}
	}

	public static Integer getAsInteger(Map<String, Object> map, String key) {
		if (!map.containsKey(key))
			return 0;
		if (map.get(key) instanceof Integer) {
			Integer value = (Integer) map.get(key);
			return value;
		} else {
			if (map.get(key) instanceof String) {
				String value = (String) map.get(key);
				return Integer.valueOf(value);
			} else {
				return 0;
			}
		}
	}

	public static Calendar getAsCalendar(Map<String, Object> map, String key) {
		if (!map.containsKey(key))
			return null;
		return parseCalendar((String) map.get(key));
	}

	public static Calendar parseCalendar(String date) {
		try {

			Calendar calendar = null;
			try {
				// "2007-12-03T10:15:30+01:00[Europe/Rome]"
				ZonedDateTime zdt = ZonedDateTime.parse(date);
				calendar = GregorianCalendar.from(zdt);
				return calendar;
			} catch (DateTimeParseException e) {
				log.trace("date discarded not in ZoneDateTime format (" + date + ")");
			}
			
			try {
				// "2001-11-27T12:00:00Z"
				Instant instant = Instant.parse(date);
				ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, ZoneId.of(ZoneOffset.UTC.getId()));
				calendar = GregorianCalendar.from(zdt);
				return calendar;
			} catch (DateTimeParseException e) {
				log.trace("date discarded not in ZoneDateTime UTC format (" + date + ")");
				
			}
			
			try {
				// "2001-11-27T12:00:00"
				LocalDateTime localDateTime = LocalDateTime.parse(date);
				calendar = Calendar.getInstance();
			    calendar.clear();
			    calendar.set(localDateTime.getYear(), localDateTime.getMonthValue()-1, localDateTime.getDayOfMonth(),
			              localDateTime.getHour(), localDateTime.getMinute(), localDateTime.getSecond());
				return calendar;
			} catch (DateTimeParseException e) {
				log.trace("date discarded not in LocalDateTime format (" + date + ")");
			}

			try {
				// "2001-11-27"
				LocalDate localDate = LocalDate.parse(date);
				calendar = Calendar.getInstance();
			    calendar.clear();
			    calendar.set(localDate.getYear(), localDate.getMonthValue()-1, localDate.getDayOfMonth());
				return calendar;
			} catch (DateTimeParseException e) {
				log.trace("date discarded not in LocalDate format (" + date + ")");
			}
			
			return calendar;
		} catch (Exception e) {
			log.warn("date discarded (" + date + ")");
			return null;
		}
	}

	private static String executeQuery(String query) {
		DefaultClientConfig clientConfig = new DefaultClientConfig();
		Client client = Client.create(clientConfig);
		client.setConnectTimeout(TIMEOUT);
		client.setReadTimeout(TIMEOUT);
		WebResource target = client.resource(query);
		// NameUsageWsClient nuws = new NameUsageWsClient(target);
		int tries = 1;
		String response = null;
		do {

			log.debug("try number {} STARTED for query {} ", tries, query);
			try {
				response = target.type(MediaType.APPLICATION_JSON).acceptLanguage(Locale.ENGLISH).get(String.class);
			} catch (Exception e) {
				log.debug("try number {} FAILED for query {} ", tries, query, e);
				try {
					if (tries < retries)
						Thread.sleep(SLEEP_TIME);
				} catch (InterruptedException e1) {
				}
			}
			tries++;
		} while (response == null && tries <= retries);
		return response;
	}

}
