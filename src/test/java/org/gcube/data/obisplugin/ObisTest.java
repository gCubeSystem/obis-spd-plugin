package org.gcube.data.obisplugin;

import java.util.Arrays;

import org.gcube.data.spd.model.exceptions.StreamException;
import org.gcube.data.spd.model.products.OccurrencePoint;
import org.gcube.data.spd.model.products.ResultItem;
import org.gcube.data.spd.obisplugin.capabilities.OccurrencesCapabilityImpl;
import org.gcube.data.spd.obisplugin.search.ResultItemSearch;
import org.gcube.data.spd.plugin.fwk.writers.ClosableWriter;
import org.gcube.data.spd.plugin.fwk.writers.ObjectWriter;
import org.junit.Test;

public class ObisTest {

	@Test
	public void searchByKey() throws Exception{
		/*ObisPlugin plugin= new ObisPlugin();
		plugin.initialize(new DatabaseCredential("jdbc:postgresql://geoserver2.i-marine.research-infrastructures.eu/obis", "postgres", "0b1s@d4sc13nc3"));
		plugin.getOccurrencesInterface().searchByScientificName("Architeuthis dux", writer);*/
		ClosableWriter<OccurrencePoint> writer = new ClosableWriter<OccurrencePoint>() {

			@Override
			public boolean isAlive() {
				return true;
			}

			@Override
			public boolean write(OccurrencePoint arg0) {
				return true;
			}

			@Override
			public boolean write(StreamException arg0) {
				return false;
			}

			@Override
			public void close() {
				// TODO Auto-generated method stub
				
			}

		};
			

		
		OccurrencesCapabilityImpl impl = new OccurrencesCapabilityImpl("https://api.obis.org/v3/");
		//impl.searchByScientificName("Cetacea", writer);
		//8f843938-1617-4c9e-be03-d1a2af8ebd89||127021
		//"3422||513384||geometry=POLYGON((30.000000%2020.000000,90.000000%2020.000000,90.000000%20180.000000,30.000000%2020.000000))"
		
		impl.getOccurrencesByProductKeys(writer, Arrays.asList("8f843938-1617-4c9e-be03-d1a2af8ebd89||127021").iterator() );
	}
	
	@Test
	public void searchByScientificName() throws Exception{
		/*ObisPlugin plugin= new ObisPlugin();
		plugin.initialize(new DatabaseCredential("jdbc:postgresql://geoserver2.i-marine.research-infrastructures.eu/obis", "postgres", "0b1s@d4sc13nc3"));
		plugin.getOccurrencesInterface().searchByScientificName("Architeuthis dux", writer);*/
		ObjectWriter<ResultItem> writer = new ObjectWriter<ResultItem>() {

			@Override
			public boolean isAlive() {
				return true;
			}

			@Override
			public boolean write(ResultItem ri) {
				System.out.println(ri);
				return true;
			}

			@Override
			public boolean write(StreamException arg0) {
				return false;
			}
		};
		
		//Balaenoptera bonaerensis
		ResultItemSearch search = new ResultItemSearch("https://api.obis.org/v3/", "Sarda sarda");
		
		search.search(writer, 50);
		
	}
	
	@Test
	public void searchByGeometry() throws Exception{
		ClosableWriter<OccurrencePoint> writer = new ClosableWriter<OccurrencePoint>() {

			@Override
			public boolean isAlive() {
				return true;
			}

			@Override
			public boolean write(OccurrencePoint arg0) {
				return true;
			}

			@Override
			public boolean write(StreamException arg0) {
				return false;
			}

			@Override
			public void close() {
				
			}

		};
			

		
		OccurrencesCapabilityImpl impl = new OccurrencesCapabilityImpl("https://api.obis.org/v3/");
		//"3422||513384||geometry=POLYGON((30.000000%2020.000000,90.000000%2020.000000,90.000000%20180.000000,30.000000%2020.000000))"
		impl.getOccurrencesByProductKeys(writer, Arrays.asList("9b787218-7138-4ffa-8ba6-36d63d391f8c||127021||geometry=POLYGON((1.6306044624659184%206.903464725745124,1.58434708579508%20-0.943627209502182,8.754369215807587%20-0.8048685858041154,8.569345073542273%206.857540488275561,1.6306044624659184%206.903464725745124))").iterator() );

		
	}
	
}
